import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:retroshare_service_flutter/retroshare_service_flutter.dart';

void main() {
  const MethodChannel channel = MethodChannel('retroshare_service_flutter');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await RetroshareServiceFlutter.platformVersion, '42');
  });
}
