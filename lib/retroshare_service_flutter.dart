
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:retroshare_service_flutter/android_config.dart';

class RetroshareServiceFlutter {
  static var isInitialized = false;
  static const MethodChannel _channel = MethodChannel('retroshare_service_flutter');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static int _androidNotificationImportanceToInt(
      AndroidNotificationImportance importance) {
    switch (importance) {
      case AndroidNotificationImportance.Low:
        return -1;
      case AndroidNotificationImportance.Min:
        return -2;
      case AndroidNotificationImportance.High:
        return 1;
      case AndroidNotificationImportance.Max:
        return 2;
      case AndroidNotificationImportance.Default:
      default:
        return 0;
    }
  }
  static Future<bool> _initialize(
      {FlutterRetroshareServiceAndroidConfig androidConfig =
      const FlutterRetroshareServiceAndroidConfig()}) async {
    isInitialized = await _channel.invokeMethod<bool>('initialize', {
      'android.notificationTitle': androidConfig.notificationTitle,
      'android.notificationText': androidConfig.notificationText,
      'android.notificationImportance': _androidNotificationImportanceToInt(
          androidConfig.notificationImportance),
      'android.notificationIconName': androidConfig.notificationIcon.name,
      'android.notificationIconDefType':
      androidConfig.notificationIcon.defType,
    }) ==
        true;
    // startRsErrorCallback = startRetroShareErrorCallback;
    return isInitialized;
  }

  static Future<bool?> startRetroshare() async {
    if(!isInitialized) {
      isInitialized = await _initialize();
    }
    final success =
    await _channel.invokeMethod<bool>('enableBackgroundExecution');
    return success;
  }

  static Future<bool?> stopRetroshare() async {
    print('Stopping Retroshare');
    final success =
    await _channel.invokeMethod<bool>('disableBackgroundExecution');
    return success;
  }
}
