import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:retroshare_service_flutter/retroshare_service_flutter.dart';
import 'package:retroshare_service_flutter_example/api_call.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _rsStatus = 'Push the button "play" to try to start rs';
  String _reqStatus = 'Use the button "Radio" to try a RS request, maybe you '
      'need to create a location first';

  bool loading = false;

  Future<void> _startRetroshare () async {
    setState(() {  loading = true; });

    final success = await RetroshareServiceFlutter.startRetroshare();
    setState(() {
      _rsStatus = success != null && success?  'Retroshare is runing' : 'RS is not running';
      loading = false;

    });
  }

  Future<void> _stopRetroshare() async {
    setState(() {  loading = true; });

    final success = await RetroshareServiceFlutter.stopRetroshare();
    setState(() {
      _rsStatus = success != null && success?  'Retroshare stopped' : 'Error stopping RS';
      loading = false;

    });
  }


  Future<void> _sendRsRequest () async {
    setState(() {  loading = true; });

    if(!RetroshareServiceFlutter.isInitialized){
      setState(() {
        _reqStatus = 'Push button "play" first to start RS';
        loading = false;

      });
      return;
    }
    try {
      rsApiCall('/rsJsonApi/version').then((value) =>
          setState(() {
            _reqStatus = value.toString();
            loading = false;

          })
      );
    } catch (err) {
      setState(() {
        _reqStatus = err.toString();
        loading = false;

      });
      rethrow;
    }
  }
  Future<void> _createLocation () async {
    setState(() {  loading = true; });
    final success = await createLocation('locationtest', '1234');
    setState(() {
      _rsStatus = success.toString();
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ListView(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Rs status :',
              style: Theme.of(context).textTheme.headline4,
            ),
            Text(
              '$_rsStatus',
            ),
            Text(
              'Rs call test:',
              style: Theme.of(context).textTheme.headline4,
            ),
            Text(
              loading ? 'Loading ' : '$_reqStatus',
            ),
            const SizedBox(height: 20.0,),
            TextButton.icon(
              onPressed: _startRetroshare,
              icon: const Icon(Icons.play_arrow),
              label: const Text('Start Retroshare'),
              style: ButtonStyle(backgroundColor: loading
                  ? MaterialStateProperty.all(Colors.grey)
                  : MaterialStateProperty.all(Colors.white)) ,
            ),
            const SizedBox(height: 20.0,),
            TextButton.icon(
              onPressed: _stopRetroshare,
              icon: const Icon(Icons.stop),
              label: const Text('STOP Retroshare'),
              style: ButtonStyle(backgroundColor: loading
                  ? MaterialStateProperty.all(Colors.grey)
                  : MaterialStateProperty.all(Colors.white)) ,
            ),
            const SizedBox(height: 20.0,),
            TextButton.icon(
              onPressed: _createLocation,
              icon: const Icon(Icons.contact_page_outlined),
              label: const Text('Create location'),
              style: ButtonStyle(backgroundColor: loading
                  ? MaterialStateProperty.all(Colors.grey)
                  : MaterialStateProperty.all(Colors.white)),
            ),
            const SizedBox(height: 20.0,),
            TextButton.icon(
              onPressed: _sendRsRequest,
              icon: const Icon(Icons.radio),
              label: const Text('Send RS request'),
              style: ButtonStyle(
                  backgroundColor: loading
                      ? MaterialStateProperty.all(Colors.grey)
                      : MaterialStateProperty.all(Colors.white)),
            ),
          ],
        ),
      ),
    );
  }
}